﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            double nmb;

            Console.Write("Your number is: ");
            double.TryParse(Console.ReadLine(), out nmb);
            int answer = 0;

            for (int i = 0; i < 3; i++)
            {
                nmb %= 1; nmb *= 10;
                answer += Convert.ToInt32(Math.Floor(nmb));
            }

            Console.WriteLine("Your answer is: " + answer);
            Console.ReadKey();
        }
    }
}
